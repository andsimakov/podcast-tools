# podcast tools

convert.py

Script convert.py converts Chapter Master generated TXT files to usual
CUE-sheet files.

Requires Python 3+.

Run as: python3 convert.py input_folder output_folder

All the parameters are mandatory. Source files should be in 'input_folder'.

========================================

csv2json.py

Convert CSV to JSON sample script

========================================

parse.py

Script parses podcast issue pages to get an issue name from a heading

========================================

tools.py

Few tools for generate, rename and rewrite a set of files according to names from an external CSV file

========================================

lbl2cue.py

Script for conversion label export text file from Audacity app to to CUE with some predefined fields