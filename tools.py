import os
import csv


def generate(first, last, file_ext):
    """
    Generates a set of files for rename test or whatever
    Function generates files to 'static' sub-folder relatively to this script
    Takes first an last issue numbers (int) to generate set of files

    :param first: int, first number in serie
    :param last: int, last number in serie
    :param file_ext: str, desirable file extention
    :return: None
    """
    counter = 0
    for i in range(first, last + 1):
        file = open('{}/{:0>3}.{}'.format(folder, str(i), file_ext), 'wt')
        # file.write('Hello world! {}'.format(str(i)))
        file.close()
        counter += 1

    print('generate(): {} item(s) generated'.format(counter))
    return None


def rename(folder, handbk):
    """
    Renames files in working folder with names from a handbook CSV file
    according to their names. Reads a handbook list to a dictionary.

    Input files should have name kinda:
    xxx.mp3 (xxx.cue). For ex.: 585.mp3 (585.cue)

    :param folder: str, working folder
    :param handbk: str, name of CSV handboor file
    :return None
    """
    with open(handbk, mode='r') as file_in:
        reader = csv.reader(file_in)
        name_list = {rows[0]: rows[1] for rows in reader}

    counter = 0
    for file_name in os.listdir(folder):
        f_id = os.path.splitext(file_name)[0]
        name_src = '{}/{}'.format(folder, file_name)
        file_ext = file_name[-3:]
        # 'A' is a cyrillic letter here
        name_dest = '{}/А-{}. {}.{}'.format(folder, f_id, name_list[f_id], file_ext)
        os.rename(name_src, name_dest)
        counter += 1

    print('rename(): {} item(s) renamed'.format(counter))
    return None


def title(folder):
    """
    Takes filename without extension and updates CUE fields TITLE and FILE with it
    :param folder: str, working folder
    :return: None
    """
    count = 0
    for file_name in os.listdir(folder):
        issue_name = '{}'.format(file_name)[:-4]

        with open('{}/{}'.format(folder, file_name), 'r+') as file_in:
            storage = []
            for line in file_in:
                storage.append(line)

            storage[0] = 'TITLE "{}"\n'.format(issue_name)
            storage[2] = 'FILE "{}.mp3" MP3\n'.format(issue_name)

            file_in.seek(0)
            for item in storage:
                file_in.write(item)
            file_in.truncate()

        count += 1

    print('title(): {} item(s) processed'.format(count))
    return None


if __name__ == "__main__":
    first = 1
    last = 10
    folder = 'static'  # Input/output folder
    handbk = 'handbook.csv'  # CSV handbook with a list of issue names for rename MP3/CUE files
    ext = 'cue'

    # generate(first, last, ext)
    rename(folder, handbk)
    # title(folder)
