import os


def label_cue(folder):
    """
    Converts text file with timestamps and labels exported from Audacity to CUE
     with some predefined fields
    :param folder: str, working folder
    :return: None
    """
    track_counter = 1
    counter = 0

    for file_name in os.listdir(folder):
        issue_name = '{}'.format(file_name)[:-4]
        file_name_in = '{}/{}'.format(folder, file_name)

        # Generate CUE sheet from source text file
        storage = []
        storage.extend(header.format(issue_name, narrator, issue_name))

        with open(file_name_in, 'r+') as file_in:
            for line in file_in:
                data_set = line.split()

                # Convert ss.xx to mm:ss:xx
                seconds = float(data_set[0])
                mm = int(seconds // 60)
                ss = str(int((seconds / 60 - mm) * 60))
                mm = str(mm)
                xx = str(round(seconds - int(seconds), 2))[2:]
                timestamp = ('{:0>2}:{:0>2}:{:0>2}'.format(mm, ss, xx))

                title = data_set[2]
                if title == 'S':
                    print('Wrong label occured')
                    return None
                elif title == '...' or title == 'Jingle':
                    performer = narrator
                    if title == 'Jingle':
                        title = 'Джингл'
                else:
                    performer = ''
                    title = '• '

                storage.append(track_record.format(track_counter, title, performer, timestamp))
                track_counter += 1

        track_counter = 1
        # Delete last empty line
        storage[len(storage) - 1] = storage[len(storage) - 1][:-1]

        # Save generated CUE sheet to a new file
        file_name_out = '{}/{}.{}'.format(folder, issue_name, output_ext)
        with open(file_name_out, 'wt') as file_out:
            for item in storage:
                file_out.write(item)

        counter += 1

    print('label_cue(): {} item(s) converted'.format(counter))
    return None


if __name__ == "__main__":
    folder = 'static'  # Input/output folder
    narrator = 'Борис Гребенщиков'
    output_ext = 'cue'

    # CUE template
    header = 'TITLE "{}"\nPERFORMER "{}"\nFILE "{}.mp3" MP3\n'
    track_record = '  TRACK {:02} AUDIO\n    TITLE "{}"\n    PERFORMER "{}"\n    INDEX 01 {:0>8}\n'

    label_cue(folder)
