import urllib.request
from bs4 import BeautifulSoup
import urllib

if __name__ == "__main__":
    # Script parses issue pages to get an issue name from a heading
    # For issues up to #602 (old URL/site!!!)

    # 584 is the number of a latest current issue
    # for i in range(1, 584 + 1):
    for i in range(595, 602 + 1):
        with urllib.request.urlopen(
                '{}{:0>2}{}'.format('http://www.aquarium.ru/misc/aerostat/aerostat', str(i), '.html')) as f:
            page = f.read(1500).decode('koi8-r')

        soup = BeautifulSoup(page, 'html.parser')

        print('{:0>3}'.format(str(i)), soup.findAll('font'))
