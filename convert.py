import sys
import os.path
import glob

if __name__ == "__main__":
    # Get input and output folders as parameters
    try:
        dir_in = sys.argv[1]
        dir_out = sys.argv[2]
    except IndexError:
        print('======================================================\n' +
              '=== Not enough parameters. Check the command line. ===\n' +
              '===      Should be input_folder output_folder      ===\n' +
              '======================================================')
        sys.exit(1)

    file_in_list = glob.glob(dir_in + '/*.txt')

    for j in range(len(file_in_list)):
        file_in_path = file_in_list[j]
        file_in_name = os.path.split(file_in_path)[1]

        # Open and read input file
        with open(file_in_path, 'rt') as file_in:
            file_in_content = file_in.read()

        # Initial cursor position
        cursor_pos = file_in_content.find('00:00:00 ')

        # Number of strings in file to convert
        string_count = file_in_content.count('\n00:')


        def timestamp_convert(timestamp):
            """ Converts time stamp to CUE time stamp
            Converts time stamp from HH:MM:ss,ms to ММ:SS:MS
            """
            timestamp = timestamp[3:].replace(',', ':')
            if len(timestamp) is 2:
                timestamp += ':00:00'
            elif len(timestamp) is 5:
                timestamp += ':00'
            else:
                timestamp += '0'
            return timestamp


        def string_parser(cursor_pos):
            """ String parser
            Parses string at current cursor position
            """
            timestamp = ''
            singer = ''
            song_title = ''
            track_title = ''

            while file_in_content[cursor_pos] != ' ':
                timestamp += file_in_content[cursor_pos]
                cursor_pos += 1
            cursor_pos += 1
            time = timestamp_convert(timestamp)

            if file_in_content[cursor_pos] != '•':
                song_flag = False
                while file_in_content[cursor_pos] != '\n':
                    track_title += file_in_content[cursor_pos]
                    cursor_pos += 1
                cursor_pos += 1
            else:
                song_flag = True
                cursor_pos += 2
                while file_in_content[cursor_pos] != '-':
                    singer += file_in_content[cursor_pos]
                    cursor_pos += 1
                singer = singer.rstrip()
                cursor_pos += 2
                while file_in_content[cursor_pos] != '\n':
                    song_title += file_in_content[cursor_pos]
                    cursor_pos += 1
                cursor_pos += 1
                track_title = '• ' + singer + ' - ' + song_title
            return time, song_flag, track_title, singer, cursor_pos


        issue_number = file_in_name[:3]
        issue_name = file_in_name[4:-10]
        notification = issue_number + ': .txt >'

        # Here's an initial cyrillic 'A'
        issue_title = 'А-' + issue_number + '. ' + issue_name
        issue_author = 'Борис Гребенщиков'
        performer = issue_author

        cue_header = [
            'TITLE "', issue_title, '"\n',
            'PERFORMER "', performer, '"\n',
            'FILE "', issue_title + '.m4a', '" WAVE', '\n']

        file_out_content = ''.join(cue_header)

        for i in range(string_count):
            track_number = '{:0>2}'.format(str(i + 1))
            time = string_parser(cursor_pos)[0]
            song_flag = string_parser(cursor_pos)[1]
            track_title = string_parser(cursor_pos)[2]
            singer = string_parser(cursor_pos)[3]
            if song_flag is True:
                performer = singer
            else:
                performer = issue_author
            cue_item = [
                '  TRACK ', track_number, ' AUDIO\n',
                '    TITLE "', track_title, '"\n',
                '    PERFORMER "', performer, '"\n',
                '    INDEX 01 ', time, '\n']
            file_out_content += ''.join(cue_item)
            cursor_pos = string_parser(cursor_pos)[4]

        # Create output file
        file_out_name = issue_title + '.cue'
        file_out_path = dir_out + '/' + file_out_name

        with open(file_out_path, 'wt') as file_out:
            file_out.write(file_out_content)
        notification += ' .cue succeed'
        print(notification)

        file_in.close()
        file_out.close()

    print('=== ' + str(j + 1) + ' file(s) converted ===')
