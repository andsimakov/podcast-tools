# Source: http://stackoverflow.com/questions/19697846/python-csv-to-json

import csv
import json

# Convert CSV to JSON sample script
csv_file = open('handbook.csv', 'r')
json_file = open('handbook.json', 'w')

fieldnames = ("ID", "IssueName")
reader = csv.DictReader(csv_file, fieldnames)
for row in reader:
    json.dump(row, json_file)
    json_file.write('\n')
